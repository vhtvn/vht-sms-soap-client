<?php

define('BASE_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);

require(BASE_DIR . 'vendor/autoload.php');

try {

    // 01. ---------- sentSms ----------
    $code    = '';
    $account = '';
    $phone   = '';
    $from    = 'VHT';
    $sms     = 'VHT - Mobile Expert';

    $smsObj  = new Vht\VhtSms($phone, true);
    $sendSms = $smsObj->sendSms($code, $account, $phone, $from, $sms);
    echo "\nPass sentSms >>>>>> ";
    echo "\n>>>>>> Status: " . $sendSms->getStatusCode() . " <<<<<<<";
    echo "\n>>>>>> Message: " . $sendSms->getMessage() . " <<<<<<<\n";

} catch (Vht\SoapClient\Exception\SmsException $ex) {
    echo("\nErr = " . $ex->getSmsExceptionErrorCode());
    echo("\nMes = " . $ex->getSmsExceptionMessage());
} catch (\Exception $ex) {
    echo "\nFinal error: " . $ex->getMessage();
    echo "\n";
}