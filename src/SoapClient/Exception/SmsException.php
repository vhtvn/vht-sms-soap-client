<?php

namespace Vht\SoapClient\Exception;

use Exception;

class SmsException extends Exception
{

    protected $message;
    protected $errorCode;

    public function __construct()
    {

    }

    function setSmsExceptionMessage($message)
    {
        $this->message = $message;
    }

    function getSmsExceptionMessage()
    {
        return $this->message;
    }

    function setSmsExceptionErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    function getSmsExceptionErrorCode()
    {
        return $this->errorCode;
    }

}