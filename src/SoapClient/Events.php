<?php
namespace Vht\SoapClient;

final class Events
{
    const REQUEST    = 'sms.soap_client.request';
    const RESPONSE   = 'sms.soap_client.response';
    const FAULT      = 'sms.soap_client.fault';
}

