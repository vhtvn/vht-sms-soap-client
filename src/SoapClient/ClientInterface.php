<?php
namespace Vht\SoapClient;

use Vht\SoapClient\Result;

/**
 * Sms API client interface
 *
 */
interface ClientInterface
{
    /**
     * Logs in to the login server and starts a client session
     *
     * @return Result\LoginResult
     * @link
     */
    public function login();

    /**
     * @param $code
     * @param $account
     * @param $phone
     * @param $from
     * @param $sms
     *
     * @return Result\SendSmsResult
     * @link http://bc.vht.com.vn:8440/vht/services/sms?wsdl
     */
    public function sendSms($code, $account, $phone, $from, $sms);

}

