<?php
namespace Vht\SoapClient;

use Vht\SoapClient\Result\LoginResult;
use Vht\Common\AbstractHasDispatcher;
use Vht\SoapClient\Soap\SoapClient;
use Vht\SoapClient\Result;
use Vht\Common\Event;

/**
 * A client for the Sms SOAP API
 *
 */
class Client extends AbstractHasDispatcher implements ClientInterface
{
    /**
     * SOAP namespace
     *
     * @var string
     */
    const SOAP_NAMESPACE = '';

    /**
     * PHP SOAP client for interacting with the Sms API
     *
     * @var SoapClient
     */
    protected $soapClient;

    /**
     * Login result
     *
     * @var Result\LoginResult
     */
    protected $loginResult;

    /**
     * Construct Sms SOAP client
     *
     * @param SoapClient $soapClient SOAP client
     *
     */
    public function __construct(SoapClient $soapClient)
    {
        $this->soapClient = $soapClient;
    }

    public function doLogin()
    {
        $loginResult = new LoginResult();
        $this->setLoginResult($loginResult);

        return $loginResult;
    }

    public function login()
    {
        return $this->doLogin();
    }

    /**
     * Get login result
     *
     * @return Result\LoginResult
     */
    public function getLoginResult()
    {
        if (null === $this->loginResult) {
            $this->login();
        }

        return $this->loginResult;
    }

    protected function setLoginResult(Result\LoginResult $loginResult)
    {
        $this->loginResult = $loginResult;
    }

    /**
     * @param $code
     * @param $account
     * @param $phone
     * @param $from
     * @param $sms
     *
     * @return Result\SendSmsResult
     * @throws \SoapFault
     */
    public function sendSms($code, $account, $phone, $from, $sms)
    {
        return $this->call(
            'sendSms',
           
                    array(
                        'code'      => $code,
                        'account'   => $account,
                        'phone'     => $phone,
                        'from'      => $from,
                        'sms'       => $sms,
                    )
            
        );
    }

    /**
     * Initialize connection
     *
     */
    protected function init()
    {
    }

    /**
     * Issue a call to Sms API
     *
     * @param string $method SOAP operation name
     * @param array  $params SOAP parameters
     *
     * @return array | $result object, such as QueryResult, SaveResult, DeleteResult.
     * @throws \Exception
     * @throws \SoapFault
     */
    protected function call($method, array $params = array())
    {
        $this->init();

        $requestEvent = new Event\RequestEvent($method, $params);
        $this->dispatch(Events::REQUEST, $requestEvent);

        try {
            $result = $this->soapClient->$method($params);
        } catch (\SoapFault $soapFault) {
            $faultEvent = new Event\FaultEvent($soapFault, $requestEvent);
            $this->dispatch(Events::FAULT, $faultEvent);

            throw $soapFault;
        }

        if (!isset($result)) {
            return array();
        }

        $this->dispatch(
            Events::RESPONSE,
            new Event\ResponseEvent($requestEvent, $result)
        );

        return $result;
    }
}