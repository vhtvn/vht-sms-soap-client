<?php

namespace Vht;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Vht\SoapClient\ClientBuilder;
use Vht\SoapClient\Result\SmsResult;

class VhtSms
{
    protected $builder = null;

    public function __construct($phone, $log = false)
    {
        $localDateTime = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        $localDateTime->setTimeZone(new \DateTimeZone('Asia/Ho_Chi_Minh'));

        $builder = new ClientBuilder(
            __DIR__.'/../private/wsdl/sms.xml'
        );

        if ($log) {
            $log = new Logger('sms');
            $log->pushHandler(
                new StreamHandler(
                    BASE_DIR . 'logs/' . $localDateTime->format('Ymd') . '/' . $phone . '_' . $localDateTime->format('His.u') . '_soap.log'
                )
            );
            $this->builder = $builder->withLog($log)->build();
        } else {
            $this->builder = $builder->build();
        }
    }

    public function sendSms($code, $account, $phone, $from, $sms)
    {
        $result = $this->builder->sendSms($code, $account, $phone, $from, $sms);
        $resultCode = $result->return;

        $smsResult = new SmsResult();
        $smsResult->setMessage('');
        $smsResult->setStatusCode($resultCode);

        return $smsResult;
    }
}